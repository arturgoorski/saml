<?php

error_reporting(E_ALL);

use Fp\Saml\Config\Idp;
use Fp\Saml\Config\Sp;
use Fp\Saml\Config;
use Fp\Saml\ServiceContainer;
use Fp\Saml\Store\RequestStateStore;
use Fp\Saml\Store\SsoState;

require_once './vendor/autoload.php';
$config = require_once './cfg.php';

$baseUrl = "https://10.57.198.42/a/";

$idpConfig = new Idp();
$idpConfig->setIdpCertificate($config['idpCertificate'])
    ->setIdpMetadataUrl($config['idpMetadataUrl']);

$spConfig = new Sp();
$spConfig->setSpCertificate($config['spCertificate'])
    ->setSpPrivateKey($config['spPrivateKey'])
    ->setSpMetadataUrl($baseUrl.'meta.php')
    ->setSpSingleLogoutUrl($baseUrl.'slo.php')
    ->setSpAssertionConsumerUrl($baseUrl.'acs.php');

$serviceConfiguration = new Config();
$serviceConfiguration->setSpConfig($spConfig)
    ->setEntityDescriptionCacheEnabled($config['entityDescriptionCache'])
    ->setCacheDir($config['entityDescriptionCacheDir'])
    ->setCacheLifetime($config['entityDescriptionCacheLifetime'])
    ->setNameIdFormat($config['nameIdFormat'])
    ->setIdpConfig($idpConfig);

$serviceContainer = ServiceContainer::getInstance();
$serviceContainer->setConfig($serviceConfiguration)
    ->setSsoStateStore(new SsoState())
    ->setRequestStateStore(new RequestStateStore());
