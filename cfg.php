<?php

require_once __DIR__.'/vendor/autoload.php';

return array(
    /**
     * @var string identity provider metadata url
     */
    'idpMetadataUrl'                 => 'https://adfs-test.future-processing.com/FederationMetadata/2007-06/FederationMetadata.xml',

    /**
     * @var string absolute path to service provider certificate
     */
    'spCertificate'                  => __DIR__.'/cert/example.org.crt',

    /**
     * @var string absolute path to service provider private key
     */
    'spPrivateKey'                   => __DIR__.'/cert/example.org.pem',

    /**
     * @var string absolute path to identity provider certificate
     */
    'idpCertificate'                 => __DIR__.'/cert/remote.crt',

    /**
     * @var string
     */
    'nameIdFormat'                   => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',

    /**
     * @var boolean indicates if cache for entity descriptions should be enabled (recommended)
     */
    'entityDescriptionCache'         => true,

    /**
     * @var int cache lifetime in seconds
     */
    'entityDescriptionCacheLifetime' => 7200,

    /**
     * @var string absolute path to cache directory (make sure that www daemon has permissions to write in this directory)
     */
    'entityDescriptionCacheDir'      => __DIR__.'/.cache',
);
