<?php

use Fp\Saml\Message\Validator\AssertionSubjectTime;
use Fp\Saml\Message\Validator\AssertionTime;
use Fp\Saml\Message\Validator\isSuccessful;
use Fp\Saml\Message\Validator\Issuer;
use Fp\Saml\Message\Validator\RequestState;
use Fp\Saml\Message\Validator\Signature;
use Fp\Saml\ResponseHandler\Authn;
use Fp\Saml\Message\Validator\SubjectConfirmationRecipient;

require_once './vendor/autoload.php';

session_start();

require_once './_bootstrap.php';

$response = new Authn();

$validators = array(
    new isSuccessful($response->getMessage()),
    new Signature($response->getMessage()),
    new Issuer($response->getMessage()),
    new AssertionTime($response->getMessage()),
    new AssertionSubjectTime($response->getMessage()),
    new SubjectConfirmationRecipient($response->getMessage()),
    new RequestState($response->getMessage())
);


foreach ($validators as $validator) {
    $validator->validate();
}

$response->handle();

//dump($response, $_SESSION);

header("Location: /a/");
