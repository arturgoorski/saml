<?php

use Fp\Saml\Request\Authn;

require_once './vendor/autoload.php';
require_once './_bootstrap.php';

$request = new Authn();
$request->getMessage()->getMessage()->setRelayState("http://onet.pl");
$request->send();

