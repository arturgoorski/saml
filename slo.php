<?php

use Fp\Saml\Message\Validator\isSuccessful;
use Fp\Saml\Message\Validator\Issuer;
use Fp\Saml\Message\Validator\RequestState;
use Fp\Saml\Message\Validator\Signature;
use Fp\Saml\RequestHandler\LogoutIdp;

require_once './vendor/autoload.php';

session_start();

require_once './_bootstrap.php';

$state = \Fp\Saml\ServiceContainer::getInstance()->getSsoStateStore()->get();

if (!$state) {
    header("Location: /a/");
}

if (isset($_GET['SAMLRequest'])) {

    $request = new LogoutIdp();

    $validators = array(
        new Signature($request->getRequestMessage()),
        new Issuer($request->getRequestMessage()),
    );

    foreach ($validators as $validator) {
        $validator->validate();
    }

    $request->handle();
} else {
    $response = new \Fp\Saml\ResponseHandler\Logout();

    $validators = array(
        new isSuccessful($response->getMessage()),
        new Signature($response->getMessage()),
        new Issuer($response->getMessage()),
        new RequestState($response->getMessage())
    );

    foreach ($validators as $validator) {
        $validator->validate();
    }

    $response->handle();
    dump($response);

    echo 'SSO state:';
    dump(\Fp\Saml\ServiceContainer::getInstance()->getSsoStateStore()->get());
    echo '<a href="sso.php">Login</a>';
}
