<?php

use Fp\Saml\Request\LogoutSp;

require_once './vendor/autoload.php';

session_start();

require_once './_bootstrap.php';

$state = \Fp\Saml\ServiceContainer::getInstance()->getSsoStateStore()->get();

if (!$state) {
    header("Location: /a/");
}

$request = new LogoutSp();
$request->send();
